<?php
    // Show Error in website
    error_reporting(-1);
    ini_set("display_errors", "1");

    // Setup Sentry connection to gitlab
    \Sentry\init(["dsn"=> "https://glet_8bf6363f2956284a1bed6cd5c8be753e@observe.gitlab.com:443/errortracking/api/v1/projects/51528039"]);
    
    try{
        // Trigger error
        throw new \Exception("First error");
    } catch (\Exception $e){
        // Send Error exception to gitlab
        \Sentry\captureException($e);
    }
